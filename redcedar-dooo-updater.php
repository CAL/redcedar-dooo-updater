<?php
/**
 * Plugin Name: Red Cedar DoOO Updater
 * Plugin URI: https://gitlab.msu.edu/CAL/redcedar-dooo-updater
 * Description: This plugin allows Red Cedar and its companion plugins to be automatically updated by WordPress when used on a Domain of One's Own WP install. Do not install this plugin on any non-DoOO environment - it will disable the auto-updater.
 * Version: 1.0
 * Author: Ben Tobey
 * Author URI: https://msu.edu/~tobeyben
 * GitLab Plugin URI: https://gitlab.msu.edu/CAL/redcedar-dooo-updater
 */
 
defined( 'ABSPATH' ) or die( 'WordPress Unavailable' );

function auto_update_specific_plugins ( $update, $item ) {
    // Array of plugin and theme slugs to always auto-update
    $plugins = array ( 
        'redcedar',
        'redcedar-dooo-updater',
        'github-updater',
    );
    if ( in_array( $item->slug, $plugins ) ) {
        return true; // Always update plugins in this array
    } else {
        return false; // Else, use the upstream updater
    }
}
add_filter( 'automatic_updater_disabled', '__return_false' );
add_filter( 'auto_update_core', '__return_false' );
add_filter( 'auto_update_translation', '__return_false' );
add_filter( 'auto_update_plugin', 'auto_update_specific_plugins', 10, 2 );
add_filter( 'auto_update_theme', 'auto_update_specific_plugins', 10, 2 );